# Esta classe transforma o corpus.txt no formato .csv, com a frequência, o formato stemmer e o formato lemman
import pandas as pd


def corpus_to_lst():
    df = pd.read_csv(f'Data/Output/OER-Corpus.csv')
    terms = []
    for i, row in df.iterrows():
        print(i)
        content = row['html']
        words = clean_text(content)
        for word in words:
            terms.append(word)
    return terms


def clean_text(content):
    text = str(content).replace("\n", "")
    # split into words
    from nltk.tokenize import word_tokenize
    tokens = word_tokenize(text.lower())
    # remove all tokens that are not alphabetic
    words = [word for word in tokens if word.isalpha()]
    return words


def counter_terms(terms):
    df = pd.DataFrame(columns=['term', 'frequency'])

    from collections import Counter
    counter = Counter(terms)
    dictionary_terms = dict(counter)
    for dictionary_term in dictionary_terms.items():
        df.loc[len(df)] = [dictionary_term[0], dictionary_term[1]]

    return df


def convert_corpus(df_terms):
    df = pd.DataFrame(columns=['term', 'frequency', 'stemmer', 'lemma'])
    for i, row in df_terms.iterrows():
        term = row['term']
        df.loc[i] = [term, row['frequency'], stemmer(term), lemma(term)]
    return df


def stemmer(term):
    from nltk.stem import PorterStemmer
    porter = PorterStemmer()
    return porter.stem(term)


def lemma(term):
    from nltk.stem import WordNetLemmatizer
    lemmatizer = WordNetLemmatizer()
    return lemmatizer.lemmatize(term)


def start():
    print('> Corpus_util_OER Started <')

    lst_terms = corpus_to_lst()
    df_terms = counter_terms(lst_terms)
    df = convert_corpus(df_terms)

    df.to_csv(r'Data/Output/IPAVL-2.csv', index=False)
    print('-> A new file was generated: Data/Output/IPAVL-2.csv')
    print('--Corpus_util_OER Finished--')