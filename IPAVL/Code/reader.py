#classe responsável por ler os REA e criar uma planilha contendo o id, a fonte e o texto do REA
import pandas as pd

path = f'Data/Input/OER'


def start():
    lst_entries = get_entries()
    df = pd.DataFrame(columns=["id", "source", "html"])
    read_html_files(lst_entries, df)

    text = load_txt()
    df.loc[len(df)] = ['ALL', 'ALL OER', text]

    df.to_csv(f'Data/Output/OER-Corpus.csv')


def get_entries():
    import os
    return os.listdir(path)


def read_html_files(lst_entries, df):
    for entry in lst_entries:
        if entry.endswith('.html') or entry.endswith('.htm'):
            html_file = open(f'{path}/{entry}', "r", encoding="utf-8", errors='ignore')
            print(entry)
            add_entry(df, entry, html_file.read())


def add_entry(df, entry, html_result):
    html = remove_tags(html_result)
    df.loc[len(df)] = [len(df) + 1, entry, html]


def remove_tags(html):
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(html, "html.parser")

    for data in soup(['style', 'script']):
        # Remove tags
        data.decompose()
    return ' '.join(soup.stripped_strings)


def load_txt():
    import os
    text = ""

    for file in os.listdir(path):
        if file.endswith('.txt'):
            full_name = os.path.join(path, file)
            with open(full_name, 'r', encoding="utf8") as file:
                text = text + file.read()
                print(len(text))
    return text
