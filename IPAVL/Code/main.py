import corpus as corpus
import corpus_util as corpus_util

# métodos que organizam o corpus dos artigos científicos
start_corpus_paper = False
start_corpus_util = False

# métodos que organizam o corpus dos REA
start_corpus_OER = False
start_corpus_util_OER = False

# métodos que aplicam os critérios. Importante: os métodos sobrescrevem os arquivos de ambos os corpus!
start_criteria_01 = False
start_criteria_02 = False
start_criteria_03 = False
start_criteria_04 = True


if start_corpus_paper:
    corpus.start()

if start_corpus_util:
    corpus_util.start()

if start_criteria_01:
    print('-- Criterion 01 started --')
    import criterion_01 as criterion_01
    criterion_01.start()
    print('-- Criterion 01 finished --')

if start_criteria_02:
    print('-- Criterion 02 started --')
    import criterion_02 as criterion_02
    criterion_02.start()
    print('-- Criterion 02 finished --')

if start_criteria_03:
    print('-- Criterion 03 started --')
    import criterion_03 as criterion_03
    criterion_03.start()
    print('-- Criterion 03 finished --')

if start_criteria_04:
    print('-- Criterion 04 started --')
    import criterion_04 as criterion_04
    criterion_04.start()
    print('-- Criterion 04 finished --')

if start_corpus_OER:
    import reader as reader
    reader.start()

if start_corpus_util_OER:
    import corpus_util_OER as cuo
    cuo.start()
