# Descrição

Diretório responsável por armazenar as planilhas que geram a IPAVL

## Estrutura de diretórios
- IPAVL-Papers: resultado final do processamento do corpus de artigos científicos.
- IPAVL-OER: resultado final do processamento dos REA.
- IPAVL: arquivo final resultante do processamento dos dados e representa o vocabulário final. Esse arquivo possui as seguintes abas: IPAVL é o resultado final, mostrando as siglas que também formam o vocabulário