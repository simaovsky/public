import pandas as pd


def start():
    df = pd.read_csv('Data/Output/tags.csv')
    df_final = pd.DataFrame(columns=['tag', 'tag_frequency', 'repositories', 'oers'])
    frequencies = df['tag'].value_counts().reset_index()
    frequency_list = frequencies.to_dict(orient='records')
    for frequency in frequency_list:
        df_aux = df[df['tag'] == frequency['tag']]

        repositories = df_aux['repository'].value_counts().reset_index()
        repositories_list = repositories.to_dict(orient='records')

        oers = df_aux['source'].value_counts().reset_index()
        oers_list = oers.to_dict(orient='records')

        df_final.loc[len(df_final)] = [frequency['tag'], frequency['count'], len(repositories_list), len(oers_list)]
    df_final.to_csv('Data/Output/analysis.csv')