import pandas as pd
from bs4 import BeautifulSoup


def start():
    df_oer = pd.read_csv('Data/Output/OER.csv')
    df = pd.DataFrame(columns=['source', 'repository', 'tag'])
    for i, row in df_oer.iterrows():
        print(i)
        tags = get_tags(row)
        for tag in tags:
            save(df, tag, row)
    df.to_csv("Data/Output/tags.csv")


def get_name(source):
    return source.split('-')[0]


def get_tags(row):
    try:
        soup = BeautifulSoup(row['html'], 'html.parser')
        all_tags = [tag.name for tag in soup.find_all()]
        return all_tags
    except:
        print(row['source'])
        return []


def save(df, tag, row):
    source = row['source']
    df.loc[len(df)] = [source, get_name(source), tag]