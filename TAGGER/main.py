start_reader = False

start_process = False

start_analysis = False

if start_reader:
    import reader as r
    r.start()

if start_process:
    import process as p
    p.start()

if start_analysis:
    import analysis as a
    a.start()
