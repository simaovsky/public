#classe responsável por ler os REA e criar uma planilha contendo o id, a fonte e o REA
import pandas as pd

path = f'Data/OER'


def start():
    lst_entries = get_entries()
    df = pd.DataFrame(columns=["id", "source", "html"])
    read_html_files(lst_entries, df)

    df.to_csv(f'Data/Output/OER.csv')


def get_entries():
    import os
    return os.listdir(path)


def read_html_files(lst_entries, df):
    for entry in lst_entries:
        if entry.endswith('.html') or entry.endswith('.htm'):
            print(entry)
            file = open(f'{path}/{entry}', "r", encoding="utf-8", errors='ignore')
            html = file.read()
            df.loc[len(df)] = [len(df) + 1, entry, html]
